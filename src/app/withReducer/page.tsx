"use client";
import AddTask from "@/components/AddTask";
import TaskList from "@/components/TaskList";
import { TasksProvider } from "@/components/contexts/TaskContext";

export default function TaskApp() {
  return (
    <>
      <TasksProvider>
        <h1>With Reducer</h1>
        <AddTask />
        <TaskList />
      </TasksProvider>
    </>
  );
}
