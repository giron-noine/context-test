"use client";
import WithFetchProvider from "@/components/WithFetchProvider";
import { FetchProvider } from "@/components/contexts/FetchContext";

export default function Home() {
  return (
    <FetchProvider>
      <div>
        <h1>Fetch</h1>
      </div>
      <WithFetchProvider />
    </FetchProvider>
  );
}
