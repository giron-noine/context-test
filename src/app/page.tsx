import Section from "@/components/Section";
import Heading from "@/components/Heading";

export default function Home() {
  return (
    <main>
      <div>
        <Section level={1}>
          <Heading>Title</Heading>
          <Section level={2}>
            <Heading>Heading</Heading>
            <Heading>Heading</Heading>
            <Heading>Heading</Heading>
            <Section level={3}>
              <Heading>Sub-heading</Heading>
              <Heading>Sub-heading</Heading>
              <Heading>Sub-heading</Heading>
              <Heading>Sub-heading</Heading>
              <Section level={4}>
                <Heading>Sub-heading</Heading>
                <Heading>Sub-heading</Heading>
                <Heading>Sub-heading</Heading>
              </Section>
            </Section>
          </Section>
        </Section>
      </div>
    </main>
  );
}
