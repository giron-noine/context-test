"use client";

import { Dispatch, createContext, useContext, useReducer } from "react";

type Todos = [
  {
    userId: number;
    id: number;
    title: string;
    completed: boolean;
  }
];

//セットするdispatchが最初はundefinedになるので、undefinedを許容する
type TodoContextType = {
  todos: Todos | undefined;
};

//コンテキストを作る
const FetchContext = createContext<TodoContextType | undefined>(undefined);

//Reducerを使ってアクションを処理する
//useStateにおける「set」と同じようなもの
type Action = { type: "fetch"; todos: Todos };
type TodoDispatch = Dispatch<Action>;

export const FetchDispatchContext = createContext<TodoDispatch | undefined>(undefined);

//ContextをまとめるProviderを作成
export function FetchProvider({ children }: { children: React.ReactNode }) {
  //useReducerの第一引数にはReducer関数、第二引数には初期値を渡す
  //useStateと同じようなもの
  const [todos, dispatch] = useReducer(fetchReducer, initialFetch);

  //childrenにはProviderで取得した値がどこからでも使えるようになる
  return (
    <>
      <FetchContext.Provider value={{ todos }}>
        <FetchDispatchContext.Provider value={dispatch}>{children}</FetchDispatchContext.Provider>
      </FetchContext.Provider>
    </>
  );
}

//Contextを使うためのカスタムフック
//Providerでラップされていない場所で使うとエラーを出す
export const useFetchContext = () => {
  const context = useContext(FetchContext);
  if (!context) {
    throw new Error("useFetchContext must be used within a FetchProvider");
  }
  return context;
};

//dispatchを使うためのカスタムフック
export function useFetchDispatchContext() {
  const context = useContext(FetchDispatchContext);
  if (!context) {
    throw new Error("useFetchDispatchContext must be used within a FetchProvider");
  }
  return context;
}

//Reducerの中身
function fetchReducer(todos: Todos, action: Action) {
  switch (action.type) {
    case "fetch": {
      return action.todos;
    }
  }
}

//初期値
//useStateの初期値と同じようなもの
const initialFetch: Todos = [
  {
    userId: 1,
    id: 1,
    title: "delectus aut autem",
    completed: false,
  },
];
