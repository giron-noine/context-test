"use client";
import { createContext } from "react";

//createContextの引数は初期値
export const LevelContext = createContext(1);
