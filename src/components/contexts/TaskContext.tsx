"use client";
import { createContext, Dispatch, useReducer, useContext } from "react";

//タスクのコンテキストを作成
type Task = {
  id: string;
  text: string;
  done: boolean;
};

type Tasks = Task[];

export const TasksContext = createContext<Tasks | undefined>(undefined);

//Reducerを使ってアクションを処理する
//Reducerは現在のstateとactionを受け取り、新しいstateを返す
//Reducerの中身は50行以降に記述
//そのReducerのdispatchをContextとして作成
type Action =
  | { type: "added"; text: string; id: number }
  | {
      type: "changed";
      task: {
        id: string;
        text: string;
        done: boolean;
      };
    }
  | { type: "deleted"; id: string };

type TodoDispatch = Dispatch<Action>;

export const TasksDispatchContext = createContext<TodoDispatch | undefined>(undefined);

//ContextをまとめるProviderを作成
export function TasksProvider({ children }: { children: React.ReactNode }) {
  const [tasks, dispatch] = useReducer(tasksReducer, initialTasks);

  return (
    <>
      <TasksContext.Provider value={tasks}>
        <TasksDispatchContext.Provider value={dispatch}>{children}</TasksDispatchContext.Provider>
      </TasksContext.Provider>
    </>
  );
}

//Reducerの中身
function tasksReducer(tasks: { id: string; text: string; done: boolean }[], action: any) {
  switch (action.type) {
    case "added": {
      return [
        ...tasks,
        {
          id: action.id,
          text: action.text,
          done: false,
        },
      ];
    }
    case "changed": {
      return tasks.map((t) => {
        if (t.id === action.task.id) {
          return action.task;
        } else {
          return t;
        }
      });
    }
    case "deleted": {
      return tasks.filter((t) => t.id !== action.id);
    }
    default: {
      throw Error("Unknown action: " + action.type);
    }
  }
}

let nextId = 3;
const initialTasks = [
  { id: 0, text: "Philosopher’s Path", done: true },
  { id: 1, text: "Visit the temple", done: false },
  { id: 2, text: "Drink matcha", done: false },
];

//カスタムフックも作れる
export function useTasks() {
  return useContext(TasksContext);
}

export function useTasksDispatch() {
  return useContext(TasksDispatchContext);
}

//使い方
// const tasks = useTasks();
// const dispatch = useTasksDispatch();
//こうすると、どこからでもtasksとdispatchを使える
