"use client";

import { TasksContext, TasksDispatchContext } from "@/components/contexts/TaskContext";
import { useContext, useState } from "react";

export default function TaskList() {
  const tasks = useContext(TasksContext);
  return (
    <ul>
      {tasks?.map((task) => (
        <li key={task.id}>
          <Task task={task} />
        </li>
      ))}
    </ul>
  );
}

function Task({ task }: { task: { id: string; text: string; done: boolean } }) {
  const [isEditing, setIsEditing] = useState(false);
  const dispatch = useContext(TasksDispatchContext);
  let taskContent;
  if (isEditing && dispatch) {
    taskContent = (
      <>
        <input
          value={task.text}
          onChange={(e) => {
            dispatch({
              type: "changed",
              task: {
                ...task,
                text: e.target.value,
              },
            });
          }}
        />
        <button onClick={() => setIsEditing(false)}>Save</button>
      </>
    );
  } else {
    taskContent = (
      <>
        {task.text}
        <button onClick={() => setIsEditing(true)}>Edit</button>
      </>
    );
  }
  return (
    <>
      {dispatch ? (
        <label>
          <input
            type="checkbox"
            checked={task.done}
            onChange={(e) => {
              dispatch({
                type: "changed",
                task: {
                  ...task,
                  done: e.target.checked,
                },
              });
            }}
          />
          {taskContent}
          <button
            onClick={() => {
              dispatch({
                type: "deleted",
                id: task.id,
              });
            }}
          >
            Delete
          </button>
        </label>
      ) : null}
    </>
  );
}
