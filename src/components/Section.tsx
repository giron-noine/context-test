"use client";

import { LevelContext } from "@/components/contexts/LevelContext";

export default function Section({ children, level }: { children: React.ReactNode; level: number }) {
  return (
    <section className="section">
      <LevelContext.Provider value={level}>{children}</LevelContext.Provider>
    </section>
  );
}
