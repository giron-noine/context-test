"use client";

import { TasksDispatchContext } from "@/components/contexts/TaskContext";
import { useContext, useState } from "react";

export default function AddTask() {
  const [text, setText] = useState("");
  const dispatch = useContext(TasksDispatchContext);
  return (
    <>
      {dispatch ? (
        <>
          <input placeholder="Add task" value={text} onChange={(e) => setText(e.target.value)} />
          <button
            onClick={() => {
              setText("");
              dispatch({
                type: "added",
                id: nextId++,
                text: text,
              });
            }}
          >
            Add
          </button>
        </>
      ) : null}
    </>
  );
}

let nextId = 3;
