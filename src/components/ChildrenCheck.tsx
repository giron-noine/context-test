import { useFetchContext } from "@/components/contexts/FetchContext";

export default function ChildrenCheck() {
  const { todos } = useFetchContext();

  return (
    <div>
      <h1>コンテキスト経由で取得できるかチェック</h1>
      <p>文字が小さいところがコンテキスト経由</p>
      <p>どこでフェッチするかが課題</p>
      <ul style={{ fontSize: 8 }}>
        {todos?.map((todo: any) => (
          <li key={todo.id}>{todo.title}</li>
        ))}
      </ul>
    </div>
  );
}
