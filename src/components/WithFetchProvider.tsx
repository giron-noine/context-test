import ChildrenCheck from "@/components/ChildrenCheck";
import { useFetchContext, useFetchDispatchContext } from "@/components/contexts/FetchContext";
import { useEffect } from "react";

export default function WithFetchProvider() {
  const { todos } = useFetchContext();
  const dispatch = useFetchDispatchContext();
  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/todos")
      .then((response) => response.json())
      .then((json) => {
        dispatch({ type: "fetch", todos: json });
      });
  }, [dispatch]);

  return (
    <div>
      <ChildrenCheck />
      <h1>このコンポーネントでフェッチしてる</h1>
      <p>ログイン状態などは一番親のコンポーネントでdispatchして渡してやるといいかも</p>
      <ul>
        {todos?.map((todo: any) => (
          <li key={todo.id}>{todo.title}</li>
        ))}
      </ul>
    </div>
  );
}
